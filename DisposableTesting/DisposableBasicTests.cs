﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DisposableTesting
{
    [TestClass]
    public class DisposableBasicTests
    {
        [TestMethod]
        public void SingleDisposable_Using_Disposed()
        {
            var disposableItem = new Mock<IDisposable>();
            using (disposableItem.Object)
            {
            }
            disposableItem.Verify(d => d.Dispose(), Times.Once);
        }

        [TestMethod]
        public void SingleDisposable_NotUsing_Disposed()
        {
            var disposableItem = new Mock<IDisposable>();
            disposableItem.Verify(d => d.Dispose(), Times.Never);
        }

        /// <summary>
        /// Verifies that property assignment with the constructor with exceptions
        /// does not actually dispose of the object. Resources
        /// could have been set up before the exception happened.
        /// </summary>
        [TestMethod]
        public void SingleDisposable_PropertyAssignmentInConstructorException_NotDisposed()
        {
            var disposableItem = new Mock<IDisposable>();

            Assert.ThrowsException<NotImplementedException>(() => 
            {
                // Property setting causes an exception, which
                // causes Dipose() to never be called.
                using (var someDisposable = new TestDisposable(disposableItem.Object) 
                                                { ExplodingProperty = "Explode" }
                )
                {

                }
            });

            // Since the outer disposable was not disposed,
            // the inner one is never disposed either
            disposableItem.Verify(d => d.Dispose(), Times.Never);
        }

        /// <summary>
        /// Verifies that property setting causing an exception AFTER 
        /// the constructor still calls Dispose(), which is expected behavior.
        /// </summary>
        [TestMethod]
        public void SingleDisposable_PropertyAssignmentLaterException_Disposed()
        {
            var disposableItem = new Mock<IDisposable>();

            Assert.ThrowsException<NotImplementedException>(() =>
            {
                using (var someDisposable = new TestDisposable(disposableItem.Object))
                {
                    someDisposable.ExplodingProperty = "Explode";
                }
            });

            // Since the property assignment was later, 
            // the objects are disposed of properly 
            disposableItem.Verify(d => d.Dispose(), Times.AtLeastOnce);
        }

        private sealed class TestDisposable : IDisposable
        {
            private readonly IDisposable _inner;

            public TestDisposable(IDisposable disposable)
            {
                _inner = disposable;
            }

            public string ExplodingProperty { 
                get { throw new NotImplementedException(); } 
                set { throw new NotImplementedException(); } 
            }

            public void Dispose()
            {
                _inner?.Dispose();
            }
        }
    }
}

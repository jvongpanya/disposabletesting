﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DisposableTesting
{
    [TestClass]
    public class NestedDisposableTests
    {
        /// <summary>
        /// Verifies that when an inner object uses and disposes an 
        /// outer disposable object, the outer object is disposed twice.
        /// </summary>
        [TestMethod]
        public void InnerDisposedOnce_MockedOuterDisposedTwice()
        {
            var mockedDisposable = new Mock<IDisposable>();
            UsesAnotherDisposable disposableItem;
            using (mockedDisposable.Object)
            {
                using (disposableItem = new UsesAnotherDisposable(mockedDisposable.Object))
                {
                }
            }
            mockedDisposable.Verify(d => d.Dispose(), Times.Exactly(2));
            Assert.AreEqual(1, disposableItem.TimesDisposed);
        }

        /// <summary>
        /// Verifies that when an inner object uses and disposes an 
        /// outer disposable object, the outer object is disposed twice.
        /// </summary>
        [TestMethod]
        public void InnerDisposedOnce_OuterDisposedTwice()
        {
            SingleDisposable singleDisposable;
            UsesAnotherDisposable disposableItem;
            using (singleDisposable = new SingleDisposable())
            {
                using (disposableItem = new UsesAnotherDisposable(singleDisposable))
                {
                }
            }
            Assert.AreEqual(2, singleDisposable.TimesDisposed);
            Assert.AreEqual(1, disposableItem.TimesDisposed);
        }


        private sealed class SingleDisposable : IDisposable
        {
            public int TimesDisposed { get; private set; }

            public void Dispose()
            {
                TimesDisposed++;
            }
        }

        private sealed class UsesAnotherDisposable : IDisposable
        {
            private IDisposable _otherDisposable;

            public UsesAnotherDisposable(IDisposable otherDisposable)
            {
                _otherDisposable = otherDisposable;
            }

            public int TimesDisposed { get; private set; }

            public void Dispose()
            {
                TimesDisposed++;
                _otherDisposable?.Dispose();
            }
        }
    }
}

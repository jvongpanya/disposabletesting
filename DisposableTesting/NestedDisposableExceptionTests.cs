﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DisposableTesting
{
    [TestClass]
    public class NestedDisposableExceptionTests
    {
        /// <summary>
        /// Verifies that if an inner disposable object has an exception, 
        /// the outer object is still disposed.
        /// </summary>
        [TestMethod]
        public void InnerDisposedException_OuterDisposed()
        {
            var mockedDisposable = new Mock<IDisposable>();
            ExceptionDisposable disposableItem;

            Assert.ThrowsException<Exception>(() =>
            {
                using (mockedDisposable.Object)
                {
                    using (disposableItem = new ExceptionDisposable())
                    {
                    }
                }
            });

            mockedDisposable.Verify(d => d.Dispose(), Times.Exactly(1));
        }

        private sealed class ExceptionDisposable : IDisposable
        {
            public ExceptionDisposable()
            {
            }

            public void Dispose()
            {
                throw new Exception();
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DisposableTesting
{
    [TestClass]
    public class ReturnValueDisposableTests
    {

        /// <summary>
        /// Verifies that an object is disposed
        /// if it is used in a separate method, 
        /// and has a return value in its using block.
        /// </summary>
        [TestMethod]
        public void SingleDisposable_Using_Returned_Disposed()
        {
            var disposableItem = new Mock<IDisposable>();

            // Dispose was not run yet
            disposableItem.Verify(d => d.Dispose(), Times.Never);
            const int returnableValue = 1;
            var someValue = MockGetIntValue(disposableItem, returnableValue);

            // Dispose called once because it was in the using block later
            Assert.AreEqual(returnableValue, someValue);
            disposableItem.Verify(d => d.Dispose(), Times.Once);
        }

        /// <summary>
        /// Verifies that an ObjectDisposedException occurs
        /// if a Task is returned from a nested using block,
        /// and requires the IDisposable.
        /// 
        /// Code smells sometimes occur if a Task does 
        /// not need to be immediately awaited. But, in this case,
        /// the ObjectDisposedException occurs if the task is from 
        /// a nested using block that requires the IDisposable, and
        /// the task is executed later.
        /// </summary>
        [TestMethod]
        public void TaskedReturnedFromUsing_Throws()
        {
            // Throws AggregateException because of task
            var exceptionResult = Assert.ThrowsException<AggregateException>(() => 
            {
                using(var timer = new Timer(TimerNotification, null,
                         1, 1))
                {
                    // Get task from another "using" 
                    // to be executed later, which is a problem
                    // because the timer gets disposed of before
                    // running the task.
                    var taskToRun = GetTimerTask(timer);
                    var result = taskToRun.Result;
                }
            });

            // The inner exception is the ObjectDisposedException expected,
            // because Timer was already disposed of before getting the result.
            Assert.IsInstanceOfType(exceptionResult.InnerException, typeof(ObjectDisposedException));
        }

        /// <summary>
        /// This verifies that awaiting a Task in a separate
        /// nested using block returns the correct result, because
        /// the nested execution completes before anything is disposed. 
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task TaskExecutedAsync_FromUsing_DoesNotThrow()
        {
            string result = string.Empty;
            using (var timer = new Timer(TimerNotification, null,
                         1, 1))
            {
                // Result is returned from task, 
                // instead of task being returned.
                // Does not throw because the entire work is done 
                // within the method, so the timer is Diposed completion.
                result = await ExecuteTimerTask(timer);
            }

            Assert.IsFalse(string.IsNullOrEmpty(result));
        }

        private int MockGetIntValue(Mock<IDisposable> someDisposable, int getValue)
        {
            using (someDisposable.Object)
            {
                return getValue;
            }
        }

        /// <summary>
        /// Returns a task from a disposable object
        /// in a "using" block, which is not advised, 
        /// because the timer passed in is disposed with the object.
        /// </summary>
        /// <param name="timer"></param>
        /// <returns></returns>
        private Task<string> GetTimerTask(Timer timer)
        {
            using (var usesTimer = new UsesTimer(timer))
            {
                return usesTimer.ChangeTime();
            }
        }

        /// <summary>
        /// Executes the task within the block, instead of 
        /// returning the task from the block to be executed later.
        /// </summary>
        /// <param name="timer"></param>
        /// <returns></returns>
        private async Task<string> ExecuteTimerTask(Timer timer)
        {
            using (var usesTimer = new UsesTimer(timer))
            {
                return await usesTimer.ChangeTime();
            }
        }

        private sealed class UsesTimer : IDisposable
        {
            private readonly Timer _timer;

            public UsesTimer(Timer timer)
            {
                _timer = timer;
            }

            public async Task<string> ChangeTime()
            {
                // This first line is required for the test
                // to fail, because it simulates waiting for 
                // an async task 
                await Task.Delay(1);
                _timer.Change(1, 1);
                return await Task.FromResult("hello");
            }

            public void Dispose()
            {
                _timer?.Dispose();
            }
        }

        private static void TimerNotification(Object obj)
        {
            Console.WriteLine("Timer event fired at {0:F}", DateTime.Now);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.IO;

namespace DisposableTesting
{
    [TestClass]
    public class StreamDisposedTwiceTests
    {
        [TestMethod]
        public void InnerMemoryStream_DisposedTwiceSafely()
        {
            UsesAnotherDisposable disposableItem;

            using (var stream = new MemoryStream())
            {
                stream.Write(new byte[] { 1, 2, 3 }, 0, 1);
                Assert.IsTrue(stream.Length > 0);
                using (disposableItem = new UsesAnotherDisposable(stream))
                {
                }
                
                // MemoryStream is disposed at this point
            }

            // MemoryStream is disposed again at this point
        }

        [TestMethod]
        public void InnerMemoryStream_DisposedTwice_And_ThrowsOnUseInOuter()
        {
            UsesAnotherDisposable disposableItem;

            Assert.ThrowsException<ObjectDisposedException>(() => 
            {
                using (var stream = new MemoryStream())
                {
                    stream.Write(new byte[] { 1, 2, 3 }, 0, 1);
                    Assert.IsTrue(stream.Length > 0);
                    using (disposableItem = new UsesAnotherDisposable(stream))
                    {
                    }

                    // MemoryStream is disposed at this point, 
                    // so it can't be used
                    var length = stream.Length;
                }
            });
        }

        [TestMethod]
        public void InnerFileStream_DisposedTwiceSafely()
        {
            UsesAnotherDisposable disposableItem;
            var guid = Guid.NewGuid().ToString();
            var fileName = $"fileStreamTests{ guid }.txt";
            var filePath = $"c:\\temp\\{ fileName }";
            using (var stream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                stream.Write(new byte[] { 1, 2, 3 }, 0, 1);
                Assert.IsTrue(stream.Length > 0);
                using (disposableItem = new UsesAnotherDisposable(stream))
                {
                }
            }

            // Clean up file
            File.Delete(filePath);
        }

        private sealed class UsesAnotherDisposable : IDisposable
        {
            private IDisposable _otherDisposable;

            public UsesAnotherDisposable(IDisposable otherDisposable)
            {
                _otherDisposable = otherDisposable;
            }

            public void Dispose()
            {
                _otherDisposable?.Dispose();
            }
        }
    }
}

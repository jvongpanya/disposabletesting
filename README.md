# Summary

These are unit tests that validate the behavior of classes implementing _IDisposable_ used within _using_ blocks.

There are comments written per test to summarize what they are testing.